@foreach($tweets as $tweet)
    <tr class="even gradeX">
        <td data-order="{{ App\Helpers\TwitterHelper::createdAt($tweet->created_at) }}">{{ App\Helpers\TwitterHelper::dataTableDateFormat($tweet->created_at) }}</td>
        <td><a href="https://twitter.com/{{ $user->screen_name }}/status/{{ $tweet->id_str }}" target="_blank">{{ $tweet->text }}</a></td>
        <td>{{ $tweet->favorite_count }}</td>
        <td>{{ $tweet->retweet_count }}</td>
    </tr>
@endforeach