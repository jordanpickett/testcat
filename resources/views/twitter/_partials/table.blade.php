<div class="panel-body">
    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
        @include('twitter._partials.table_headers')
        <tbody>
            @include('twitter._partials.table_body')
        </tbody>
    </table>
    <!-- /.table-responsive -->
</div>
<!-- /.panel-body -->