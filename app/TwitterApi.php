<?php

namespace App;

class TwitterApi
{
    private $access_token;
    private $access_token_secret;
    private $consumer_key;
    private $consumer_secret;

    public $url;
    public $getField;
    public $requestMethod;

    public function __construct()
    {
        $this->access_token = env('ACCESS_TOKEN');
        $this->access_token_secret = env('ACCESS_TOKEN_SECRET');
        $this->consumer_key = env('CONSUMER_KEY');
        $this->consumer_secret = env('CONSUMER_SECRET');
    }

    public function getGetField()
    {
        return $this->getField;
    }

    public function setGetField($params)
    {
        $this->getField = '?' . http_build_query($params, '', '&');

        return $this;
    }

    public function buildOauth($url, $requestMethod)
    {
//        if (!in_array(strtolower($requestMethod), array('post', 'get', 'put', 'delete')))
//        {
//            throw new Exception('Request method must be either POST, GET or PUT or DELETE');
//        }
        $consumer_key              = $this->consumer_key;
        $consumer_secret           = $this->consumer_secret;
        $oauth_access_token        = $this->access_token;
        $oauth_access_token_secret = $this->access_token_secret;
        $oauth = array(
            'oauth_consumer_key' => $consumer_key,
            'oauth_nonce' => time(),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_token' => $oauth_access_token,
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0'
        );
        $getfields = $this->getGetfield();
        if (!is_null($getfields))
        {
            $getfields = str_replace('?', '', explode('&', $getfields));
            foreach ($getfields as $g)
            {
                $split = explode('=', $g);
                /** In case a null is passed through **/
                if (isset($split[1]))
                {
                    $oauth[$split[0]] = urldecode($split[1]);
                }
            }
        }

        $base_info = $this->buildBaseString($url, $requestMethod, $oauth);
        $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
        $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        $oauth['oauth_signature'] = $oauth_signature;
        $this->url           = $url;
        $this->requestMethod = $requestMethod;
        $this->oauth         = $oauth;
        return $this;
    }

    public function performRequest($return = true, $curlOptions = array())
    {
        if (!is_bool($return))
        {
            throw new Exception('performRequest parameter must be true or false');
        }
        $header =  array($this->buildAuthorizationHeader($this->oauth), 'Expect:');

        $getfield = $this->getGetfield();
        //$postfields = $this->getPostfields();
        if (in_array(strtolower($this->requestMethod), array('put', 'delete')))
        {
            $curlOptions[CURLOPT_CUSTOMREQUEST] = $this->requestMethod;
        }
        $options = $curlOptions + array(
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_HEADER => false,
                CURLOPT_URL => $this->url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 10,
            );
//        if (!is_null($postfields))
//        {
//            $options[CURLOPT_POSTFIELDS] = http_build_query($postfields, '', '&');
//        }
//        else
//        {
//            if ($getfield !== '')
//            {
//                $options[CURLOPT_URL] .= $getfield;
//            }
//        }

        if ($getfield !== '')
        {
            $options[CURLOPT_URL] .= $getfield;
        }
        $feed = curl_init();
        curl_setopt_array($feed, $options);
        $json = curl_exec($feed);
        $this->httpStatusCode = curl_getinfo($feed, CURLINFO_HTTP_CODE);
        if (($error = curl_error($feed)) !== '')
        {
            curl_close($feed);
            throw new \Exception($error);
        }
        curl_close($feed);
        return $json;
    }

    private function buildAuthorizationHeader(array $oauth)
    {
        $return = 'Authorization: OAuth ';
        $values = array();
        foreach($oauth as $key => $value)
        {
            if (in_array($key, array('oauth_consumer_key', 'oauth_nonce', 'oauth_signature',
                'oauth_signature_method', 'oauth_timestamp', 'oauth_token', 'oauth_version'))) {
                $values[] = "$key=\"" . rawurlencode($value) . "\"";
            }
        }
        $return .= implode(', ', $values);
        return $return;
    }

    private function buildBaseString($baseURI, $method, $params)
    {
        $return = array();
        ksort($params);
        foreach($params as $key => $value)
        {
            $return[] = rawurlencode($key) . '=' . rawurlencode($value);
        }
        return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $return));
    }
}
