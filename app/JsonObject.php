<?php

namespace App;


class JsonObject
{
    public function __construct($json = false) {
        if ($json) $this->set($json);
    }

    public function set($data) {
        foreach ($data AS $key => $value) {
            if (is_array($value)) {
                $sub = new JsonObject;
                $sub->set($value);
                $value = $sub;
            }
            $this->{$key} = $value;
        }
    }
}
