<?php

namespace App\Helpers;

use DateTime;

class TwitterHelper
{
    public static $baseUrl = 'https://api.twitter.com/1.1/';
    public static $requestsUrl = [
        'time_line' => 'statuses/user_timeline.json',
        'followers' => 'followers/ids.json'
    ];

    public static function createdAt($timestamp)
    {
        $date = new DateTime($timestamp);
        return $date->format('Y/m/d');
    }

    public static function dataTableDateFormat($timestamp) {
        $date = new DateTime($timestamp);
        return $date->format('F j, Y');
    }
}
