<?php

namespace App\Console\Commands\Twitter;

use App\Tweet;
use App\TwitterApi;
use Illuminate\Console\Command;

class TwitterRequestCommand extends Command
{
    protected $url, $method, $params, $excludeHashtag;

    public function __construct($url, $method, $params, $excludeHashtag)
    {
        $this->url = $url;
        $this->method = $method;
        $this->params = $params;
        $this->excludeHashtag = $excludeHashtag;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $twitter = new TwitterApi();
        $tweets = $twitter->setGetfield($this->params)
            ->buildOauth($this->url, $this->method)
            ->performRequest();
        $tweets = json_decode($tweets, true);
        $collection = [];
        foreach($tweets as $tweet)
        {
            $modelTweet = new Tweet($tweet);
            $includeTweet = true;
            if($this->excludeHashtag) {
                if(!empty((array)(($hashtags = $modelTweet->entities->hashtags)))) {
                    foreach($hashtags as $hashtag) {
                        if(str_contains($this->excludeHashtag, $hashtag->text)) {
                            $includeTweet = false;
                        }
                    }
                }
            }
            $includeTweet ? $collection[] = $modelTweet : false;
        }

        return $collection;
    }
}
