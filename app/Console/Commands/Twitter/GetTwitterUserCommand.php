<?php

namespace App\Console\Commands\Twitter;

use App\JsonObject;
use App\User;
use Illuminate\Console\Command;

class GetTwitterUserCommand extends Command
{
    protected $user;

    public function __construct(JsonObject $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = new User((array)$this->user);
        return $user;
    }
}
