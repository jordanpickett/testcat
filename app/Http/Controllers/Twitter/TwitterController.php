<?php

namespace App\Http\Controllers\Twitter;

use App\Console\Commands\Twitter\GetTwitterUserCommand;
use App\Console\Commands\Twitter\TwitterRequestCommand;
use App\Helpers\TwitterHelper;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Mockery\CountValidator\Exception;

class TwitterController extends Controller
{

    public function index()
    {
        $input = Input::all();

        $url = TwitterHelper::$baseUrl.TwitterHelper::$requestsUrl['time_line'];
        $params = array();
        $params['screen_name'] = 'ScottStapp';
        $params['count'] = '100';
        $params['include_rts'] = false;
        $requestMethod = 'GET';
        //dd($input);

        try {
            if($tweets = $this->dispatch(new TwitterRequestCommand($url, $requestMethod, $params, array_get($input, 'exclude-hashtag')))) {
                if($user = $this->dispatch(new GetTwitterUserCommand($tweets[0]->user))) {
                    //dd($tweets);
                    return view('twitter.index', [
                        'tweets' => $tweets,
                        'user' => $user
                    ]);
                }
                else {
                    return 'Twitter user not found';
                }
            }
            else {
                return 'failed';
        }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
