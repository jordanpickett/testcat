<?php

namespace App\Traits;


use stdClass;

trait StdClassTrait
{

    /**
     * Make a model from stdObject.
     *
     * @param  stdClass $std
     * @param  array    $fill
     * @param  boolean  $exists
     * @return \Illuminate\Database\Eloquent\Model
     */
    public static function newFromStd(stdClass $std, $fill = ['*'], $exists = true)
    {
        $instance = new static;

        $values = ($fill == ['*'])
            ? (array) $std
            : array_intersect_key( (array) $std, array_flip($fill));

        // fill attributes and original arrays
        $instance->setRawAttributes($values, true);

        $instance->exists = $exists;

        return $instance;
    }
}
